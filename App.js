import React from 'react'
import { NativeRouter, Routes, Route } from 'react-router-native'
import { Text } from 'react-native'
import Redirect from './src/Redirect'
import Main from './src/Main'
import MyTracks from './src/MyTracks'
import Calendar from './src/Calendar'
import Rec from './src/Rec'
import Groups from './src/Groups'
import Me from './src/Me'
import Settings from './src/Settings'
import style from './src/style'

const Home = () => <Text>HOME</Text>

const App = () => {

  return (
    <NativeRouter>
      <Routes>
        <Route path="/" Component={ Main }>
          <Route path="my-tracks" Component={ MyTracks } />

          <Route path="calendar" Component={ Calendar } />

          <Route path="rec" Component={ Rec } />

          <Route path="groups" Component={ Groups } />

          <Route path="me" Component={ Me } />

          <Route path="settings" Component={ Settings } />

          <Route index element={ <Redirect to="/my-tracks" /> } />

          <Route path="*" element={ <Redirect to="/my-tracks" /> } />
        </Route>

        <Route path="auth" element={ <Text>auth</Text> } />
      </Routes>
    </NativeRouter>
  )
}

export default App
// import { View, Text } from 'react-native'
// import { NativeRouter, Routes, Route, Outlet, Link, useParams } from 'react-router-native'
// 
// const Home = () => <Text>HOME</Text>
// 
// const About = () => <Text>ABOUT</Text>
// 
// const Topic = () => {
//   const { id } = useParams()
// 
//   return <Text>mierda { id }</Text>
// }
// 
// const Topics = ( props ) => (
//   <View>
//     <Text>TOPICS</Text>
// 
//     <Link to="/topics/1">
//       <Text>1</Text>
//     </Link>
// 
//     <Link to="/topics/2">
//       <Text>2</Text>
//     </Link>
// 
//     <Outlet />
//   </View>
// )
// 
// const App = () => (
//   <NativeRouter>
//     <View>
//       <View>
//         <Link to="/">
//           <Text>Home</Text>
//         </Link>
// 
//         <Link to="/about">
//           <Text>about</Text>
//         </Link>
// 
//         <Link to="/topics">
//           <Text>topics</Text>
//         </Link>
//       </View>
// 
//       <Routes>
//         <Route exact path="/" element={ <Home /> } />
//         <Route path="/about" element={ <About /> } />
//         <Route path="/topics" Component={ Topics }>
//           <Route path="/topics/:id" Component={ Topic } />
//           <Route exact path="/topics" element={ <Text>select topic</Text> } />
//         </Route>
//       </Routes>
//     </View>
//   </NativeRouter>
// )
// 
// export default App
