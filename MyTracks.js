import React, { useState } from 'react'
import { Button, Text, ScrollView, View, TouchableOpacity } from 'react-native'
import { Feather, Ionicons, Entypo, MaterialCommunityIcons } from '@expo/vector-icons'
import style from './style'

import TrackList from './TrackList'

function App() {
  const [ isOpen, setIsOpen ] = useState( false )

  return (
    <View style={ style.containter }>
      <View style={ [
        style.hs,
        style.bar
      ] }>
        <Text
          style={ [
            style.one,
            style.spacingLeft,
            style.title
          ] }
          numberOfLines={ 1 }
          ellipsizeMode="tail"
        >my tracks</Text>

        <TouchableOpacity style={ [ style.squareButton ] }>
          <Feather name="cloud" size={ 32 } />
        </TouchableOpacity>

        <TouchableOpacity style={ [ style.squareButton ] }>
          <Ionicons name="water-outline" size={ 32 } />
        </TouchableOpacity>

        <Text style={ [
          style.selfCenter
        ] }>34%</Text>

        <TouchableOpacity style={ [ style.squareButton ] }>
          <Feather name="settings" size={ 32 } />
        </TouchableOpacity>
      </View>

      <TrackList />

      { /* footer */ }
      <View style={ [
        style.tabs,
        style.hs,
        style.spaceBetween
      ] }>
        <TouchableOpacity style={ [
          style.tab
        ] }>
          <Entypo name="flow-line" size={ 32 } style={ [
            style.rotate90
          ] } />
        </TouchableOpacity>

        <TouchableOpacity style={ [
          style.tab
        ] }>
          <Feather name="calendar" size={ 32 } />
        </TouchableOpacity>

        <TouchableOpacity style={ [
          style.tab
        ] }>
          <MaterialCommunityIcons name="record-circle-outline" size={ 32 } />
        </TouchableOpacity>

        <TouchableOpacity style={ [
          style.tab
        ] }>
          <Feather name="users" size={ 32 } />
        </TouchableOpacity>

        <TouchableOpacity style={ [
          style.tab
        ] }>
          <Feather name="user" size={ 32 } />
        </TouchableOpacity>

      </View>
      { /* end footer */ }
    </View>
  )
}

export default App
