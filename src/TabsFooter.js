import React from 'react'
import { useNavigate } from 'react-router-native'
import { View, TouchableOpacity } from 'react-native'
import { Feather, Entypo, MaterialCommunityIcons } from '@expo/vector-icons'
import style from './style'

const TabsFooter = () => {
  const navigate = useNavigate()

  const go = ( to ) => {
    navigate( to )
  }

  return (
    <View style={ [
      style.tabs,
      style.vs,
      style.spaceBetween
    ] }>
      <TouchableOpacity
        onPress={ () => go( '/my-tracks' ) }
        style={ [ style.tab ] }
      >
        <Entypo name="flow-line" size={ 32 } style={ [
          style.rotate90
        ] } />
      </TouchableOpacity>

      <TouchableOpacity
        onPress={ () => go( '/calendar' ) }
        style={ [ style.tab ] }
      >
        <Feather name="calendar" size={ 32 } />
      </TouchableOpacity>

      <TouchableOpacity
        onPress={ () => go( '/rec' ) }
        style={ [ style.tab ] }
      >
        <MaterialCommunityIcons name="record-circle-outline" size={ 32 } />
      </TouchableOpacity>

      <TouchableOpacity
        onPress={ () => go( '/groups' ) }
        style={ [ style.tab ] }
      >
        <Feather name="users" size={ 32 } />
      </TouchableOpacity>

      <TouchableOpacity
        onPress={ () => go( '/me' ) }
        style={ [ style.tab ] }
      >
        <Feather name="user" size={ 32 } />
      </TouchableOpacity>

    </View>
  )
}

export default TabsFooter
