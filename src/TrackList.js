import React from 'react'
import { ScrollView } from 'react-native'
import style from './style'

import TrackItem from './TrackItem'

const routes = [
  {
    name: 'tizayuca con las brujas roller',
    at: 1688842462380,
    distance: 30.1
  },
  {
    name: 'basilica con brujas roller',
    at: 1688842462380,
    distance: 26.3
  },
  {
    name: 'torre del pantalon con balam rollers',
    at: 1688842462380,
    distance: 43.1
  }
]

function TrackList() {
  return (
    <ScrollView style={ [
      style.one,
      style.hs,
      style.spacing
    ] }>
      {
        routes.map( ( route, index ) => {
          return (
            <TrackItem
              key={ index }
              data={ route }
            />
          )
        } )
      }
    </ScrollView>
  )
}

export default TrackList
