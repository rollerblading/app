import React, { useEffect } from 'react'
import { useNavigate } from 'react-router-native'

const Redirect = ( { to } ) => {
  const navigate = useNavigate()

  useEffect( () => {
    navigate( to )
  } )

  return <></>
}

export default Redirect
