import React from 'react'
import { useNavigate } from 'react-router-native'
import { View, TouchableOpacity } from 'react-native'
import { Feather, Entypo, MaterialCommunityIcons } from '@expo/vector-icons'
import style from './style'

const RecFooter = () => {
  const navigate = useNavigate()

  const go = ( to ) => {
    navigate( to )
  }

  return (
    <View style={ [
      style.tabs,
      style.vs,
      style.vsReverse
    ] }>
      <TouchableOpacity
        style={ [ style.tab ] }
      >
        <MaterialCommunityIcons name="pin-outline" size={ 32 } />
      </TouchableOpacity>

      <TouchableOpacity
        style={ [ style.tab ] }
      >
        <Feather name="camera" size={ 32 } />
      </TouchableOpacity>

      <TouchableOpacity
        style={ [ style.tab ] }
      >
        <Feather name="video" size={ 32 } />
      </TouchableOpacity>

      <TouchableOpacity
        style={ [ style.tab ] }
      >
        <Feather name="mic" size={ 32 } />
      </TouchableOpacity>

    </View>
  )
}

export default RecFooter
