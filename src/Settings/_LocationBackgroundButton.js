import { View, Text, TouchableOpacity } from 'react-native'
import * as Location from 'expo-location'
import style from '../style'
import { MaterialCommunityIcons } from '@expo/vector-icons'

const LocationBackgroundButton = () => {
  const [status, requestPermission] = Location.useBackgroundPermissions( { Accuracy: 'Highest' } )

  if ( ! status )
    return null

  return <View style={ [
    style.vs,
    style.centered
  ] }>

    <View style={ [
      style.one,
      style.hs
    ] }>
      <Text>background</Text>

      <Text>background location access</Text>
    </View>

    { status.granted ?
      <MaterialCommunityIcons name="square-rounded" size={ 32 } />
    :
      <TouchableOpacity onPress={ requestPermission }>
        <MaterialCommunityIcons name="square-rounded-outline" size={ 32 } />
      </TouchableOpacity>
    }

  </View>
}

export default LocationBackgroundButton
