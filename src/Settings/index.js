import React from 'react'
import { useNavigate } from 'react-router-native'
import { View, Text, TouchableOpacity, ScrollView } from 'react-native'
import { Feather } from '@expo/vector-icons'
import LocationAccessButton from './_LocationAccessButton'
import LocationBackgroundButton from './_LocationBackgroundButton'
import style from '../style'

const Settings = () => {
  const navigate = useNavigate()

  const save = () => {
    navigate( '/my-tracks' )
  }

  return (
    <View style={ [
      style.hs,
      style.one
    ] }>
      <View style={ [
        style.vs,
        style.bar
      ] }>
        <Text
          style={ [
            style.one,
            style.spacingLeft,
            style.title
          ] }
          numberOfLines={ 1 }
          ellipsizeMode="tail"
        >settings</Text>

        <TouchableOpacity
          onPress={ () => save() }
          style={ [ style.square ] }
        >
          <Feather name="save" size={ 32 } />
        </TouchableOpacity>

      </View>

      <ScrollView style={ [
        style.one,
        style.hs,
        style.spacing
      ] }>

        <View style={ [
          style.hs
        ] }>

          <Text>permissions</Text>

          <View style={ [
            style.hs,
            style.wideSpacingLeft
          ] }>

            <LocationAccessButton />

            <LocationBackgroundButton />

          </View>

        </View>

      </ScrollView>

    </View>
  )
}

export default Settings
