import { StyleSheet } from 'react-native'
import Constants from 'expo-constants'

const base = 7

const style = StyleSheet.create( {
  containter: {
    paddingTop: Constants.statusBarHeight,
    flex: 1
  },
  vs: {
    display: 'flex',
    flexDirection: 'row'
  },
  vsReverse: {
    display: 'flex',
    flexDirection: 'row-reverse'
  },
  hs: {
    display: 'flex',
    flexDirection: 'column',
  },
  hsReverse: {
    display: 'flex',
    flexDirection: 'column-reverse'
  },
  bar: {
    height: 49
  },
  title: {
    fontSize: 28,
    lineHeight: 42
  },
  squareButton: {
    width: 49,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  wideSpacingLeft: {
    paddingLeft: 35
  },
  spacing: {
    padding: 7
  },
  spacingLeft: {
    paddingLeft: 7
  },
  centered: {
    alignItems: 'center',
  },
  justified: {
    justifyContent: 'center'
  },
  one: {
    flex: 1
  },
  two: {
    flex: 2
  },
  three: {
    flex: 3
  },
  four: {
    flex: 4
  },
  five: {
    flex: 5
  },
  fsOne: {
    fontSize: base
  },
  fsTwo: {
    fontSize: base * 2
  },
  fsThree: {
    fontSize: base * 3
  },
  fsFour: {
    fontSize: base * 4
  },
  fsFive: {
    fontSize: base * 5
  },
  fsSix: {
    fontSize: base * 6
  },
  fsSeven: {
    fontSize: base * 7
  },
  fsBig: {
    fontSize: base * 14
  },
  gap: {
    gap: base
  },
  gapTwo: {
    gap: base * 2
  },
  gapThree: {
    gap: base * 3
  },
  pink: {
    backgroundColor: '#f89a9b'
  },
  blue: {
    backgroundColor: '#81afed'
  },
  selfEnd: {
    alignSelf: 'flex-end'
  },
  selfStart: {
    alignSelf: 'flex-start'
  },
  selfCenter: {
    alignSelf: 'center'
  },
  rotate90: {
    transform: [ {
      rotate: '90deg'
    } ]
  },
  tabs: {
    height: 70
  },
  tab: {
    width: 70,
    height: 70,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  spaceBetween: {
    justifyContent: 'space-between'
  },
  square: {
    width: 49,
    height: 49,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  }
} )

export default style
