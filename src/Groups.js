import React from 'react'
import TabsFooter from './TabsFooter'
import { View, Text } from 'react-native'
import style from './style'

const Groups = () => {

  return (
    <View style={ [
      style.hs,
      style.one
    ] }>
      <View style={ [
        style.vs,
        style.bar
      ] }>
        <Text
          style={ [
            style.one,
            style.spacingLeft,
            style.title
          ] }
          numberOfLines={ 1 }
          ellipsizeMode="tail"
        >groups</Text>
      </View>

      <View style={ [
        style.one
      ] }>
      </View>

      <TabsFooter />
    </View>
  )
}

export default Groups
