import React, { useState, createContext } from 'react'

const RouterContext = createContext()

function Router( props ) {
  const [ stack, setStack ] = useState( [] )

  const push = ( route, params ) => {
    console.log( { route, params } )
  }

  return (
    <RouterContext.Provider value={ {
      push
    } }>
    </RouterContext.Provider>
  )
}

function Route( { path, element } ) {
}
