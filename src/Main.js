import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { Outlet } from 'react-router-native'
import Db from './Db'
import { Feather, Entypo, MaterialCommunityIcons } from '@expo/vector-icons'
import style from './style'

const Main = () => {
  return <Db>
    <View style={ [
      style.containter
    ] }>

      <Outlet />

    </View>
  </Db>
}

export default Main
