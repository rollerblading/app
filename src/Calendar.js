import React, { useState } from 'react'
import TabsFooter from './TabsFooter'
import { View, Text, TouchableOpacity } from 'react-native'
import { Feather } from '@expo/vector-icons'
import moment from 'moment'
import style from './style'

const Calendar = () => {
  const [ today ] = useState( moment() )
  const [ currentMonth, setCurrentMonth ] = useState( moment( today ).startOf( 'month' ) )

  const next = () => {
    console.log( 'next', currentMonth )
    setCurrentMonth( moment( currentMonth ).add( 1, 'month' ) )
  }

  const back = () => {
    setCurrentMonth( moment( currentMonth ).subtract( 1, 'month' ) )
  }

  const renderDays = ( fromDate = moment() ) => {
    const startOfMonth = moment( fromDate ).startOf( 'month' )
    const weekday = moment( startOfMonth ).weekday()
    const startDate = moment( startOfMonth ).subtract( weekday, 'days' )
    const nextMonth = moment( startOfMonth ).add( 1, 'month' )

    let rows = []

    // for ( let i = 0; i <= 3; i++ ) {
    while( startDate.isSameOrBefore( nextMonth ) ) {
      let days = []

      for ( let j = 0; j < 7; j ++ ) {
        days.push(
          <Text
            style={ [
              style.one
            ] }
          >{ startDate.format( 'D' ) }</Text>
        )

        startDate.add( 1, 'day' )
      }

      rows.push(
        <View style={ [
          style.one,
          style.vs,
          style.gap
        ] }>
          { days }
        </View>
      )

      console.log( startDate.format(), nextMonth.format() )
    }

    return rows
  }

  const renderHeader = () => {
    return [
      'sun',
      'mon',
      'tue',
      'wed',
      'thu',
      'fri',
      'sat'
    ]
      .map( day => <Text
        key={ day }
        style={ [
          style.one
        ] }
      >
        { day }
      </Text> )
  }

  return (
    <View style={ [
      style.hs,
      style.one
    ] }>
      <View style={ [
        style.vs,
        style.bar
      ] }>
        <Text
          style={ [
            style.one,
            style.spacingLeft,
            style.title
          ] }
          numberOfLines={ 1 }
          ellipsizeMode="tail"
        >{ currentMonth.format( 'MMMM YYYY' ) }</Text>

        <TouchableOpacity
          style={ [ style.squareButton ] }
          onPress={ back }
        >
          <Feather name="arrow-left" size={ 32 } />
        </TouchableOpacity>

        <TouchableOpacity
          style={ [ style.squareButton ] }
          onPress={ next }
        >
          <Feather name="arrow-right" size={ 32 } />
        </TouchableOpacity>
      </View>

      <View style={ [
        style.one,
        style.hs,
        style.spacing
      ] }>

        <View style={ [
          style.pink,
          style.vs,
          style.gap
        ] }>
          { renderHeader() }
        </View>

        { renderDays( currentMonth ) }

      </View>

      <TabsFooter />
    </View>
  )
}

export default Calendar
