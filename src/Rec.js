import React from 'react'
import RecFooter from './RecFooter'
import { View, Text, TouchableOpacity } from 'react-native'
import { useNavigate } from 'react-router-native'
import { Feather, MaterialCommunityIcons } from '@expo/vector-icons'
import style from './style'

const Rec = () => {
  const navigate = useNavigate()

  const save = () => {
    navigate( '/my-tracks' )
  }

  return (
    <View style={ [
      style.hs,
      style.one
    ] }>
      <View style={ [
        style.vs,
        style.bar
      ] }>
        <Text
          style={ [
            style.one,
            style.spacingLeft,
            style.title
          ] }
          numberOfLines={ 1 }
          ellipsizeMode="tail"
        >rec</Text>

        <TouchableOpacity
          onPress={ () => save() }
          style={ [ style.square ] }
        >
          <Feather name="save" size={ 32 } />
        </TouchableOpacity>
      </View>

      <View style={ [
        style.one,
        style.hs,
        style.gapThree,
        style.centered,
        style.justified
      ] }>
        <View>
          <View style={ [
            style.hs,
            style.centered
          ] }>
            <Text style={ [
              style.fsThree
            ] }>distance</Text>

            <Text style={ [
              style.fsBig
            ] }>14</Text>

            <Text style={ [
              style.fsThree
            ] }>km</Text>
          </View>
        </View>

        <View>
          <View style={ [
            style.hs,
            style.centered
          ] }>
            <Text style={ [
              style.fsThree
            ] }>speed</Text>

            <Text style={ [
              style.fsBig
            ] }>17</Text>

            <Text style={ [
              style.fsThree
            ] }>km/h</Text>
          </View>
        </View>

      </View>

      <RecFooter />
    </View>
  )
}

export default Rec
