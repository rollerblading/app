import React, { createContext } from 'react'
import { openDatabase } from 'expo-sqlite'

const DbContext = createContext()

const db = openDatabase( 'db' )

function Db( props ) {

  return (
    <DbContext.Provider
      value={ {
        db
      } }
    >
      { props.children }
    </DbContext.Provider>
  )
}

const withDb = ( ComponentAlias ) => {
  return props => (
    <DbContext.Consumer>
      { context => <ComponentAlias { ...props } withDb={ context } /> }
    </DbContext.Consumer>
  )
}

export {
  withDb,
  DbContext
}

export default Db
