import React from 'react'
import { useNavigate } from 'react-router-native'
import { Text, View, TouchableOpacity } from 'react-native'
import { Feather, Ionicons } from '@expo/vector-icons'
import style from './style'

import TrackList from './TrackList'
import TabsFooter from './TabsFooter'

function App() {
  const navigate = useNavigate()

  const go = ( to ) => {
    navigate( to )
  }

  return (
    <View style={ [
      style.hs,
      style.one
    ] }>
      <View style={ [
        style.vs,
        style.bar
      ] }>
        <Text
          style={ [
            style.one,
            style.spacingLeft,
            style.title
          ] }
          numberOfLines={ 1 }
          ellipsizeMode="tail"
        >my tracks</Text>

        <TouchableOpacity style={ [ style.squareButton ] }>
          <Feather name="cloud" size={ 32 } />
        </TouchableOpacity>

        <TouchableOpacity style={ [ style.squareButton ] }>
          <Ionicons name="water-outline" size={ 32 } />
        </TouchableOpacity>

        <Text style={ [
          style.selfCenter
        ] }>34%</Text>

        <TouchableOpacity
          style={ [ style.squareButton ] }
          onPress={ () => go( '/settings' ) }
        >
          <Feather name="settings" size={ 32 } />
        </TouchableOpacity>
      </View>

      <TrackList />

      <TabsFooter />
    </View>
  )
}

export default App
