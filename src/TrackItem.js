import React, { useState } from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import { Feather, Ionicons, Entypo } from '@expo/vector-icons'
import moment from 'moment'
import style from './style'

function TrackItem( { data } ) {
  const [ isOpen, setIsOpen ] = useState( false )

  const toggle = () => {
    setIsOpen( ! isOpen )
  }

  return (
    <View style={ [
      style.vs,
      style.spacing
    ] }>
      <View style={ [
        style.one,
        style.hs
      ] }>
        <TouchableOpacity onPress={ toggle }>
          <View style={ [
            style.one,
            style.vs
          ] }>
            <View style={ [
              style.one,
              style.hs
            ] }>
              <Text style={ [ style.fsThree ] }>{ data.name }</Text>

              <Text style={ [ style.fsTwo ] }>{ moment( data.at ).format( 'll' ) }</Text>
            </View>

            { ! isOpen ? (
              <View style={ [
                style.vs,
                style.spacing,
                style.centered,
                style.justified,
                style.gap
              ] }>
                <Text style={ [
                  style.fsFour
                ] }>{ data.distance }</Text>

                <Text style={ [
                  style.fsTwo
                ] }>km</Text>
              </View>
            ) : null }
          </View>
        </TouchableOpacity>

        { isOpen ? (
          <View style={ [
            style.one,
            style.hs,
            style.gap
          ] }>
            { /* row (average and max speed) */ }
            <View style={ [
              style.one,
              style.vs
            ] }>
              <View style={ [
                style.one,
                style.hs
              ] }>
                <Text style={ [ style.fsTwo ] }>average speed</Text>

                <View style={ [
                  style.vs,
                  style.gap
                ] }>
                  <Text style={ [
                    style.fsFour
                  ] }>{ 17.3 }</Text>

                  <Text style={ [
                    style.fsTwo,
                    style.selfEnd
                  ] }>km/h</Text>
                </View>
              </View>

              <View style={ [
                style.one,
                style.hs
              ] }>
                <Text style={ [ style.fsTwo ] }>maximum speed</Text>

                <View style={ [
                  style.vs,
                  style.gap
                ] }>
                  <Text style={ [
                    style.fsFour
                  ] }>{ 48 }</Text>

                  <Text style={ [
                    style.fsTwo,
                    style.selfEnd
                  ] }>km/h</Text>
                </View>
              </View>
            </View>
            { /* end row */ }

            { /* row (time in motion and distance) */ }
            <View style={ [
              style.one,
              style.vs
            ] }>
              <View style={ [
                style.one,
                style.hs
              ] }>
                <Text style={ [ style.fsTwo ] }>time in motion</Text>

                <View style={ [
                  style.vs,
                  style.gap
                ] }>
                  <Text style={ [
                    style.fsFour
                  ] }>1:04:42</Text>
                </View>
              </View>

              <View style={ [
                style.one,
                style.hs
              ] }>
                <Text style={ [ style.fsTwo ] }>distance</Text>

                <View style={ [
                  style.vs,
                  style.gap
                ] }>
                  <Text style={ [
                    style.fsFour
                  ] }>{ data.distance }</Text>

                  <Text style={ [
                    style.fsTwo,
                    style.selfEnd
                  ] }>km</Text>
                </View>
              </View>
            </View>
            { /* end row */ }

            <View style={ [
              style.vs,
              style.vsReverse,
              style.blue
            ] } >
              <TouchableOpacity style={ [ style.square, style.pink ] }>
                <Feather name="map" size={ 28 } />
              </TouchableOpacity>

              <TouchableOpacity style={ [ style.square, style.pink ] }>
                <Feather name="share-2" size={ 28 } />
              </TouchableOpacity>

              <TouchableOpacity style={ [ style.square, style.pink ] }>
                <Feather name="trash-2" size={ 28 } />
              </TouchableOpacity>

            </View>
          </View>
        ) : null }

      </View>

    </View>
  )
}

export default TrackItem
