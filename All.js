import React, { useState, useEffect } from 'react'
import * as Location from 'expo-location'
import * as TaskManager from 'expo-task-manager'
import { Button, Text, View, StyleSheet } from 'react-native';
import axios from 'axios'
import { openDatabase } from 'expo-sqlite'

const db = openDatabase( 'db' )

const TASK_FETCH_LOCATION = 'TASK_FETCH_LOCATION'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
})

function App( props ) {
  const [ hasPermissions, setHasPermissions ] = useState( false )
  const [ isInitialized, setIsInitialized ] = useState( false )
  const [ isRecovered, setIsRecovered ] = useState( false )
  const [ isTracking, setIsTracking ] = useState( false )
  const [ currentTrack, setCurrentTrack ] = useState()

  useEffect( () => {
    ( async() => {
      if ( ! hasPermissions )
        await requestPermissions()

      if ( ! isInitialized )
        await bootstrap()

      if ( ! isRecovered )
        await recovery()
    } )()

    TaskManager.defineTask(
      TASK_FETCH_LOCATION,
      async ( { data: { locations }, err } ) => {
        if ( err ) {
          console.error( err )
          return
        }

        const values = locations
          .map( loc => ( {
            at: loc.timestamp,
            ...loc.coords,
            trackId: currentTrack
          } ) )
          .map( point => {
            return [
              'at',
              'latitude',
              'longitude',
              'accuracy',
              'altitude',
              'altitudeAccuracy',
              'heading',
              'speed',
              'trackId'
            ]
              .map( k => {
                return point[ k ]
              } )
              .join( ', ' )
          } )
          .join( '), (' )

        const query = `INSERT INTO points ( at, latitude, longitude, accuracy, altitude, altitudeAccuracy, heading, speed, trackId ) VALUES ( ${ values } )`

        console.log( query )
        await executeSql( query )
      }
    )

  } )

  const requestPermissions = async () => {
    console.log( 'request permissions executed' )

    const { status: foregroundStatus } = await Location.requestForegroundPermissionsAsync()
    if ( foregroundStatus !== 'granted' ) {
      console.error( 'Permission to access location was denied' )
      return
    }

    const { status: backgroundStatus } = await Location.requestBackgroundPermissionsAsync()
    if ( backgroundStatus !== 'granted' ) {
      console.error( 'Permission to access background location was denied' )
      return
    }

    setHasPermissions( true )
  }

  const hasStartedUpdates = () => {
    return Location.hasStartedLocationUpdatesAsync( TASK_FETCH_LOCATION )
  }

  const recovery = async () => {
    console.log( 'recovery' )
    setIsRecovered( true )
    const isListening = await hasStartedUpdates()

    if ( ! isListening ) return

    const { id } = await getLastTrack()
    if ( ! id ) {
      await _stopTracking()
      return
    }

    setIsTracking( true )
    setCurrentTrack( id )
  }

  const _stopTracking = async () => {
    console.log( '_stopTracking' )
    if ( await hasStartedUpdates() )
      await Location.stopLocationUpdatesAsync( TASK_FETCH_LOCATION )
  }

  const getLastTrack = async () => {
    console.log( 'getLastTrack' )
    const { rows: { _array } } = await executeSql( 'SELECT id FROM tracks ORDER BY id DESC limit 1' )
    return _array.shift() || {}
  }

  const executeSql = async ( query, args = [] ) => {
    console.log( 'executeSql' )
    return new Promise( ( resolve, reject ) => {
      db.transaction( tx => {
        tx.executeSql(
          query,
          args,
          ( _, resultSet ) => {
            // console.log( { query, args, resultSet } )
            // console.log( { resultSet } )
            resolve( resultSet )
          },
          ( _, error ) => {
            // console.error( { query, args, error } )
            reject( error )
          }
        )
      } )
    } )
  }

  const bootstrap = async () => {
    console.log( 'bootstrap' )

    try {
      await executeSql( `
        CREATE TABLE IF NOT EXISTS tracks (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          name VARCHAR(300),
          startAt INTEGER,
          endAt INTEGER
        )
      ` )
      await executeSql( `
        CREATE TABLE IF NOT EXISTS points (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          at INTEGER,
          latitude REAL,
          longitude REAL,
          accuracy REAL,
          altitude REAL,
          altitudeAccuracy REAL,
          heading REAL,
          speed REAL,
          trackId INTEGER NOT NULL
        )
      ` )

      const { rows: { _array } } = await executeSql( 'SELECT name FROM sqlite_master WHERE type = \'table\'' )
      console.log( _array )

      setIsInitialized( true )
    } catch ( err ) {
      console.error( err )
    }

  }

  const clean = async () => {
    console.log( 'clean' )
    await executeSql( `
      DROP TABLE tracks;
    ` )
    await executeSql( `
      DROP TABLE points;
    ` )

    await _stopTracking()
    setIsInitialized( false )
    setIsTracking( false )
    setCurrentTrack( null )
  }

  const log = async ( data ) => {
    try {
      await axios( {
        url: 'http://192.168.7.157:6667/eagle/update',
        method: 'POST',
        headers: {
          Authorization: 'I_AM_ROOT'
        },
        data
      } )
    } catch ( err ) {
      console.error( err )
    }
  }

  const createTrack = async () => {
    console.log( 'createTrack' )
    const at = new Date()
    const name = at.toISOString()

    return await executeSql(
      'INSERT INTO tracks (name, startAt) values (?, ?)',
      [
        name,
        at.valueOf()
      ]
    )
  }

  const start = async () => {
    console.log( 'start' )

    const isListening = await hasStartedUpdates()

    if ( isListening ) {
      const { id } = await getLastTrack()
      if ( ! id ) {
        await _stopTracking()
        return
      }

      setIsTracking( true )
      setCurrentTrack( id )
      return
    }

    setIsTracking( true )
    const { insertId } = await createTrack()
    setCurrentTrack( insertId )

    Location.startLocationUpdatesAsync(
      TASK_FETCH_LOCATION,
      {
        accuracy: Location.Accuracy.High,
        distanceInterval: 1,
        deferredUpdatesInterval: 1000,
        foregroundService: {
          notificationTitle: 'Using your location',
          notificationBody: 'To turn off, go back to the app and switch something off'
        }
      }
    )
  }

  const stop = async () => {
    console.log( 'stop' )
    setIsTracking( false )

    await _stopTracking()

    const at = new Date()
    await executeSql(
      'UPDATE tracks SET endAt = ? WHERE id = ?',
      [
        at.valueOf(),
        currentTrack
      ]
    )
  }

  return (
    <View style={styles.container}>
      { isInitialized ?
        <Button onPress={ clean } title="clean"/>
      : <Button onPress={ bootstrap } title="bootstrap"/> }
      { isTracking ? (
        <>
          <Text>recording track #{ currentTrack }</Text>
          <Button onPress={ stop } title="stop"/>
        </>
      ) : 
        <Button onPress={ start } title="start"/>
      }
    </View>
  )
}

export default App
