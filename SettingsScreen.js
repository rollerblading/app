import React from 'react'
import { Button, Text, View } from 'react-native'

function SettingsScreen( { navigation } ) {
  return (
    <View>
      <Text>Settings screen</Text>
      <Button title="Settings" onPress={ () => navigation.goBack() } />
    </View>
  )
}

export default SettingsScreen
