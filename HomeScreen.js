import React from 'react'
import { Button, Text, View } from 'react-native'

function HomeScreen( { navigation } ) {
  return (
    <View>
      <Text>Home screen</Text>
      <Button title="Settings" onPress={ () => navigation.navigate( 'Settings' ) } />
    </View>
  )
}

export default HomeScreen
